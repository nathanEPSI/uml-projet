package com.epsi.umlprojet.models;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Adress extends ContactDetail {

    public Adress() {
        super();
        this.contactType = "Adress";
    }

    private static final String ADRESS_REGEX = ".{16,}";

    private static final Pattern ADDRESS_PATTERN = Pattern.compile(ADRESS_REGEX);

    @Override
    public boolean validate() {
        Matcher matcher = ADDRESS_PATTERN.matcher(this.value);
        return matcher.matches();
    }
    
}
