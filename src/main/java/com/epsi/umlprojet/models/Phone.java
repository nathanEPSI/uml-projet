package com.epsi.umlprojet.models;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Phone extends ContactDetail {

    public Phone() {
        super();
        this.contactType = "Phone";
    }

    private static final String PHONE_REGEX = "(0|\\+33|0033)[1-9][0-9]{8}";
    private static final Pattern PHONE_PATTERN = Pattern.compile(PHONE_REGEX);

    @Override
    public boolean validate() {
        Matcher matcher = PHONE_PATTERN.matcher(this.value);
        return matcher.matches();
    }

}
