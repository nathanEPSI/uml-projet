package com.epsi.umlprojet.models;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Website extends ContactDetail {

    public Website() {
        super();
        this.contactType = "Website";
    }

    private static final String WEBSITE_REGEX = "^(https?://)?([A-Za-z0-9-])+(\\.[A-Za-z]{2,})+(\\/.*)?$";
    private static final Pattern WEBSITE_PATTERN = Pattern.compile(WEBSITE_REGEX);

    @Override
    public boolean validate() {
        Matcher matcher = WEBSITE_PATTERN.matcher(this.value);
        return matcher.matches();
    }
    
}
