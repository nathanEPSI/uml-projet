package com.epsi.umlprojet.models;

import java.util.Scanner;

public class User {
    private String login;
    private String password;
    private String jwtToken;

    private static Scanner sc = new Scanner(System.in);

    public User() {
        System.out.println("Please input the user login :");
        this.login = sc.nextLine();
        System.out.println("Please input the user password :");
        this.password = sc.nextLine();
    }

    public String getLogin() {
        return this.login;
    }
}
