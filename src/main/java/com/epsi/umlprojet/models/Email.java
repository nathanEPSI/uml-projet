package com.epsi.umlprojet.models;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Email extends ContactDetail {

    public Email() {
        super();
        this.contactType = "Email";
    }

    private static final String EMAIL_REGEX = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$";
    private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX);

    @Override
    public boolean validate() {
        Matcher matcher = EMAIL_PATTERN.matcher(this.value);
        return matcher.matches();
    }
    
}
