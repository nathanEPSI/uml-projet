package com.epsi.umlprojet.models;

public abstract class ContactDetail {
    protected String contactType;
    protected String value;
    
    public abstract boolean validate();

    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    public String getContactType() {
        return this.contactType;
    }
}
