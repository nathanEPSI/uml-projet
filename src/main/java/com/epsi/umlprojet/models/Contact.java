package com.epsi.umlprojet.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Contact {
    private static final Scanner sc = new Scanner(System.in);
    private final String name;
    private final List<ContactDetail> adresses;

    public Contact() {
        System.out.println("Please input the name of the contact :");
        this.name = sc.nextLine();
        this.adresses = new ArrayList<>();

        boolean setAdress = true;
        while(setAdress) {
            System.out.println("Would you like to add an adress to this contact ? (y/n)");
            String answer = sc.nextLine().toLowerCase();
            if(answer.equals("y")) {
                this.addAdress();
            } else if(answer.equals("n")) {
                setAdress = false;
            } else {
                System.out.println("Sorry, I did not understand this command, please try again.");
            }
        }
    }

    public void addAdress() {
        ContactDetail contactToAdd = null;
        
        int userAns = 0;

        while(userAns < 1 || userAns > 4) {
            System.out.printf(
                    """
                            What type of adress do you want to add to %s:
                            1- Adress (69 rue de la soif)
                            2- Email (email@email.com)
                            3- Phone (0600000000)
                            4- Website (https://www.example.com)
                            %n""", this.name);
            userAns = sc.nextInt();
        }

        switch (userAns) {
            case 1 -> contactToAdd = new Adress();
            case 2 -> contactToAdd = new Email();
            case 3 -> contactToAdd = new Phone();
            case 4 -> contactToAdd = new Website();
            default -> addAdress();
        }

        // otherwise next input is skipped by scanner
        sc.nextLine();

        System.out.printf("Please input the value of the %s :\n", contactToAdd.getContactType());
        contactToAdd.setValue(sc.nextLine());

        while (!contactToAdd.validate()) {
            System.out.printf("Value incorrect for this contact type (%s), please try again :\n", contactToAdd.getContactType());
            contactToAdd.setValue(sc.nextLine());
        }
        
        this.adresses.add(contactToAdd);
    }

    public void displayContact() {
        System.out.println(name + " :");
        for (ContactDetail adresse : this.adresses) {
            System.out.println("- " + adresse.getContactType() + " = " + adresse.getValue());
        }
    }
}
