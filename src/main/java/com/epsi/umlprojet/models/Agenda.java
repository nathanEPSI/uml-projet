package com.epsi.umlprojet.models;

import java.util.ArrayList;
import java.util.List;

public class Agenda {
    private User owner;
    private List<Contact> contacts;

    public Agenda() {
        this.contacts = new ArrayList<>();
    }

    public void setContacts(List<Contact> contactsToAdd) {
        this.contacts.addAll(contactsToAdd);
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public void displayAgenda() {
        System.out.println("############## AGENDA ##############");
        System.out.println(String.format("Owner = %s", this.owner.getLogin()));
        System.out.println("############# CONTACTS #############");
        for (Contact contact : this.contacts) {
            contact.displayContact();
        }
        System.out.println("####################################");
    }
}
