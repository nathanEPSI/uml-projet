import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.epsi.umlprojet.models.*;

public class UmlProjetApplication {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {


		System.out.println("Let's create an agenda.");
		Agenda agenda = new Agenda();

		System.out.println("First, we have to create a user who is going to be the owner of the agenda.");
		User owner = new User();
		agenda.setOwner(owner);

		System.out.println("Next, we have to add contacts to your agenda's contacts.");
		List<Contact> contacts = new ArrayList<>();

		boolean addContacts = true;
		while(addContacts) {
			System.out.println("Would you like to add a contact to this agenda ? (y/n)");
            String answer = sc.nextLine().toLowerCase();
            if(answer.equals("y")) {
                Contact contactToAdd = new Contact();
				contacts.add(contactToAdd);
            } else if(answer.equals("n")) {
                addContacts = false;
            } else {
				System.out.println("Sorry, I did not understand this command, please try again.");
			}
		}

		agenda.setContacts(contacts);
		
		
		System.out.println("We have successfully created the agenda, let's display the agenda :");
		agenda.displayAgenda();
	}

}
